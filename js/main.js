var app = angular.module('ChartsApp', ['highcharts-ng']);

app.controller('MainCtrl', ['$scope', function($scope){
    var colors = Highcharts.getOptions().colors;
    var selectedColors = [];
    var getColor = function(){
        var rn = Math.floor((Math.random() * 10));
        var ind = selectedColors.indexOf(colors[rn]);
        if(ind === -1){
            selectedColors.push(colors[rn]);
            return colors[rn];
        }else{
            if(selectedColors.length >= 10){
                selectedColors = [];
            }
            return getColor();
        }
    };




    $scope.types = ['pie', 'bar', 'area', 'line', 'column', 'spline', 'areaspline', 'scatter', ''];

	$scope.chartConfig = {
        series: [],
        loading: true
    };

    var onDrillDown = function(e){
        if (!e.seriesOptions) {
            var chart = this;
            var dd = {
                'Students': {
                    name: 'Students',
                    data: [
                        ['Cats', 2],
                        ['Dogs', 4],
                        ['Cows', 9],
                        ['Sheep', 0],
                        ['Pigs', 2]
                    ]
                }
            };
            series = dd[e.point.name];
            chart.hideLoading();
            chart.addSeriesAsDrilldown(e.point, series);
        }
    };

    $scope.initChart = function(title){
        $scope.chartConfig = {
            options: {
                chart: {
                    events: {
                        drilldown: onDrillDown
                    }
                }
            },
            xAxis: {
                type: 'category'
            },
            title: {
                text: title
            },
            loading: true,
            series: []
        }
    };

    var setType = function(type){
        if(type === 'donut'){
            $scope.chartConfig.plotOptions = {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white',
                            textShadow: '0px 1px 2px black'
                        }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%']
                }
            };
        }else{
            $scope.chartConfig.chat.type = type;
        }
    };

    $scope.initChart('Test Chart');
    $scope.setType('donut');

    $scope.addSeries = function(data){
        $scope.chartConfig.series.push(data);

    };



    $scope.addSeries({
        name: 'Jan',
        color: getColor(),
        data: [
            { name: 'Students', y: 187, drilldown: true },
            { name: 'Teachers', y: 136, drilldown: true },
            { name: 'Staff', y: 60},
            { name: 'Others', y: 29}
        ]
    });

    $scope.add = function(){
        var data = {};
        data.name = $scope.newSet.name;
        data.color = getColor()
        data.data = [$scope.newSet.data1, $scope.newSet.data2, $scope.newSet.data3, $scope.newSet.data4];
        $scope.chartConfig.series.push(data);
        console.log(data);
    };

    $scope.dd = function(){
        console.log($scope.getColor());
    };
}]);

app.controller('PieCtrl', ['$scope', function($scope){
    $scope.chartConfig = {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'Browser<br>shares',
            align: 'center',
            verticalAlign: 'middle',
            y: 50
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            innerSize: '50%',
            data: [
                ['Firefox',   45.0],
                ['IE',       26.8],
                ['Chrome', 12.8],
                ['Safari',    8.5],
                ['Opera',     6.2],
                {
                    name: 'Others',
                    y: 0.7,
                    dataLabels: {
                        enabled: false
                    }
                }
            ]
        }]
    };
}]);
